const cards = [
  {
    id: '2♣',
    url: './img/clovers/2_clovers.svg',
    color: '♣',
    number: 2,
  },
  {
    id: '3♣',
    url: './img/clovers/3_clovers.svg',
    color: '♣',
    number: 3,
  },
  {
    id: '4♣',
    url: './img/clovers/4_clovers.svg',
    color: '♣',
    number: 4,
  },
  {
    id: '5♣',
    url: './img/clovers/5_clovers.svg',
    color: '♣',
    number: 5,
  },
  {
    id: '6♣',
    url: './img/clovers/6_clovers.svg',
    color: '♣',
    number: 6,
  },
  {
    id: '7♣',
    url: './img/clovers/7_clovers.svg',
    color: '♣',
    number: 7,
  },
  {
    id: '8♣',
    url: './img/clovers/8_clovers.svg',
    color: '♣',
    number: 8,
  },
  {
    id: '9♣',
    url: './img/clovers/9_clovers.svg',
    color: '♣',
    number: 9,
  },
  {
    id: '10♣',
    url: './img/clovers/10_clovers.svg',
    color: '♣',
    number: 10,
  },
  {
    id: 'J♣',
    url: './img/clovers/J_clovers.svg',
    color: '♣',
    number: 11,
  },
  {
    id: 'Q♣',
    url: './img/clovers/Q_clovers.svg',
    color: '♣',
    number: 12,
  },
  {
    id: 'K♣',
    url: './img/clovers/K_clovers.svg',
    color: '♣',
    number: 13,
  },
  {
    id: 'A♣',
    url: './img/clovers/A_clovers.svg',
    color: '♣',
    number: 14,
  },
  {
    id: '2♦',
    url: './img/tiles/2_tiles.svg',
    color: '♦',
    number: 2,
  },
  {
    id: '3♦',
    url: './img/tiles/3_tiles.svg',
    color: '♦',
    number: 3,
  },
  {
    id: '4♦',
    url: './img/tiles/4_tiles.svg',
    color: '♦',
    number: 4,
  },
  {
    id: '5♦',
    url: './img/tiles/5_tiles.svg',
    color: '♦',
    number: 5,
  },
  {
    id: '6♦',
    url: './img/tiles/6_tiles.svg',
    color: '♦',
    number: 6,
  },
  {
    id: '7♦',
    url: './img/tiles/7_tiles.svg',
    color: '♦',
    number: 7,
  },
  {
    id: '8♦',
    url: './img/tiles/8_tiles.svg',
    color: '♦',
    number: 8,
  },
  {
    id: '9♦',
    url: './img/tiles/9_tiles.svg',
    color: '♦',
    number: 9,
  },
  {
    id: '10♦',
    url: './img/tiles/10_tiles.svg',
    color: '♦',
    number: 10,
  },
  {
    id: 'J♦',
    url: './img/tiles/J_tiles.svg',
    color: '♦',
    number: 11,
  },
  {
    id: 'Q♦',
    url: './img/tiles/Q_tiles.svg',
    color: '♦',
    number: 12,
  },
  {
    id: 'K♦',
    url: './img/tiles/K_tiles.svg',
    color: '♦',
    number: 13,
  },
  {
    id: 'A♦',
    url: './img/tiles/A_tiles.svg',
    color: '♦',
    number: 14,
  },
  {
    id: '2♥',
    url: './img/hearts/2_hearts.svg',
    color: '♥',
    number: 2,
  },
  {
    id: '3♥',
    url: './img/hearts/3_hearts.svg',
    color: '♥',
    number: 3,
  },
  {
    id: '4♥',
    url: './img/hearts/4_hearts.svg',
    color: '♥',
    number: 4,
  },
  {
    id: '5♥',
    url: './img/hearts/5_hearts.svg',
    color: '♥',
    number: 5,
  },
  {
    id: '6♥',
    url: './img/hearts/6_hearts.svg',
    color: '♥',
    number: 6,
  },
  {
    id: '7♥',
    url: './img/hearts/7_hearts.svg',
    color: '♥',
    number: 7,
  },
  {
    id: '8♥',
    url: './img/hearts/8_hearts.svg',
    color: '♥',
    number: 8,
  },
  {
    id: '9♥',
    url: './img/hearts/9_hearts.svg',
    color: '♥',
    number: 9,
  },
  {
    id: '10♥',
    url: './img/hearts/10_hearts.svg',
    color: '♥',
    number: 10,
  },
  {
    id: 'J♥',
    url: './img/hearts/J_hearts.svg',
    color: '♥',
    number: 11,
  },
  {
    id: 'Q♥',
    url: './img/hearts/Q_hearts.svg',
    color: '♥',
    number: 12,
  },
  {
    id: 'K♥',
    url: './img/hearts/K_hearts.svg',
    color: '♥',
    number: 13,
  },
  {
    id: 'A♥',
    url: './img/hearts/A_hearts.svg',
    color: '♥',
    number: 14,
  },
  {
    id: '2♠',
    url: './img/pikes/2_pikes.svg',
    color: '♠',
    number: 2,
  },
  {
    id: '3♠',
    url: './img/pikes/3_pikes.svg',
    color: '♠',
    number: 3,
  },
  {
    id: '4♠',
    url: './img/pikes/4_pikes.svg',
    color: '♠',
    number: 4,
  },
  {
    id: '5♠',
    url: './img/pikes/5_pikes.svg',
    color: '♠',
    number: 5,
  },
  {
    id: '6♠',
    url: './img/pikes/6_pikes.svg',
    color: '♠',
    number: 6,
  },
  {
    id: '7♠',
    url: './img/pikes/7_pikes.svg',
    color: '♠',
    number: 7,
  },
  {
    id: '8♠',
    url: './img/pikes/8_pikes.svg',
    color: '♠',
    number: 8,
  },
  {
    id: '9♠',
    url: './img/pikes/9_pikes.svg',
    color: '♠',
    number: 9,
  },
  {
    id: '10♠',
    url: './img/pikes/10_pikes.svg',
    color: '♠',
    number: 10,
  },
  {
    id: 'J♠',
    url: './img/pikes/J_pikes.svg',
    color: '♠',
    number: 11,
  },
  {
    id: 'Q♠',
    url: './img/pikes/Q_pikes.svg',
    color: '♠',
    number: 12,
  },
  {
    id: 'K♠',
    url: './img/pikes/K_pikes.svg',
    color: '♠',
    number: 13,
  },
  {
    id: 'A♠',
    url: './img/pikes/A_pikes.svg',
    color: '♠',
    number: 14,
  },
];


export const getCards = () => {
  const myCards = [];
  while (myCards.length < 10) {
    const chooseCard = Math.floor(Math.random() * 51);
    const getCard = cards[chooseCard].id;
    if (!myCards.includes(getCard)) {
      myCards.push(getCard);
    }
  }
  return myCards;
};


export const getCardImage = (id) => cards.find((x) => x.id === id).url;


export const getCardNumber = (id) => cards.find((x) => x.id === id).number;


export const getCardColor = (id) => cards.find((x) => x.id === id).color;
