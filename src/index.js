import { betOne, betMax, startGame, drawFirstCards, amounts }
  from '../src/events/buttons-events.js';
import { selectCard } from './events/card-events.js';

document.addEventListener('DOMContentLoaded', () => {
  document.addEventListener('click', (event) => {
    if (event.target.id === 'betOne') {
      betOne();
    }

    if (event.target.id === 'betMax') {
      betMax();
    }

    if (event.target.id === 'draw' && amounts.bet) {
      drawFirstCards();
    }
    if (event.target.classList.contains('cards')) {
      selectCard(event.target.getAttribute('id'),
          event.target.getAttribute('name'));
    }
  });
  startGame();
});
