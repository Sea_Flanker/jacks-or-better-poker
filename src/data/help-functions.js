/**
 * @author Simeon Mladenov
 * @param {String} id Object ID
 * @param {String} value HTML to insert
 */
export const insertHTML = (id, value) => {
  document.getElementById(id).innerHTML = value;
};

/**
* @description Clear the html in object
* @param {String} id Object ID
*/
export const clearHTML = (id) => {
  document.getElementById(id).innerHTML = '';
};

/**
* @description Program stop for a while
* @param {Number} ms time in milliseconds
* @return {Promise}
*/
export const waitSeconds = async (ms) => {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
};
