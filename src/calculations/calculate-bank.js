export const calculateBank = (combination, bet) => {
  if (combination === 'nothing') {
    return 0;
  }
  const position = (bet / 100) - 1;
  combination = combination.toUpperCase().split(' ').join('_');
  const win = profit.find((x) => x.id === combination).prize[position];
  // console.log(`${win} x ${bet} = ${bet * win}`);
  return (bet * win);
};

const profit = [
  {
    id: 'JACKS_OR_BETTER',
    prize: [1, 2, 3, 4, 5],
  },
  {
    id: 'TWO_PAIR',
    prize: [2, 4, 6, 8, 10],
  },
  {
    id: 'THREE_OF_A_KIND',
    prize: [3, 6, 9, 12, 15],
  },
  {
    id: 'STRAIGHT',
    prize: [4, 8, 12, 16, 20],
  },
  {
    id: 'FLUSH',
    prize: [6, 12, 18, 24, 30],
  },
  {
    id: 'FULL_HOUSE',
    prize: [9, 18, 27, 36, 45],
  },
  {
    id: 'FOUR_OF_A_KIND',
    prize: [25, 50, 75, 100, 125],
  },
  {
    id: 'STRAIGHT_FLUSH',
    prize: [50, 100, 200, 400, 800],
  },
  {
    id: 'ROYAL_FLUSH',
    prize: [250, 500, 1000, 2000, 4000],
  },
];
