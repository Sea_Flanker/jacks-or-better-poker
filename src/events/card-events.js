import { getCardImage } from '../data/cards.js';
import { myDeck } from './buttons-events.js';

export const selectCard = (cardId, cardName) => {
  if (myDeck.step !== 1) {
    return;
  }

  if (myDeck.changeDeck.includes(cardName)) {
    document.getElementById(cardId).src = getCardImage(cardName);
    myDeck.changeDeck = myDeck.changeDeck.filter((x) => x !== cardName);
  } else {
    document.getElementById(cardId).src = './img/card_back.svg';
    myDeck.changeDeck.push(cardName);
  }
};

export const changeCards = () => {
  const extraCards = myDeck.extraDeck.slice(0, myDeck.changeDeck.length);
  let counter = 0;

  myDeck.finalDeck = myDeck.firstDeck.reduce((acc, el) => {
    if (myDeck.changeDeck.includes(el)) {
      acc.push(extraCards[counter]);
      counter++;
    } else {
      acc.push(el);
    }
    return acc;
  }, []);
};
