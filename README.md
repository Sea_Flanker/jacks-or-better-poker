# Jacks or Better poker
### Описание
Игра от типа Jacks or Better

### Правила
Всички валидни за покера комбинации носят печалба, единственото изключение е двойката (чифтът), който трябва да бъде минимум валета.
### Залог
Залогът се определя предварително преди раздаването на картите и не може да бъде променян след като се получат първите пет карти. Също така не може да бъде и намаляван след като като е повишен. Тоест ако е зададен да бъде 200, няма как да бъде свален на 100. Максималният залог е 500.
