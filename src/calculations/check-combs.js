import { getCardColor, getCardNumber } from '../data/cards.js';

export const checkCombinations = (cardDeck) => {
  let result = 'nothing';
  result = jacksOrBetter(cardDeck) ? result = 'Jacks or Better' : result;
  result = twoPairs(cardDeck) ? result = 'Two Pair' : result;
  result = threeOfaKind(cardDeck) ? result = 'Three of a Kind' : result;
  result = straight(cardDeck) ? result = 'Straight' : result;
  result = flush(cardDeck) ? result = 'Flush' : result;
  result = fullHouse(cardDeck) ? result = 'Full House' : result;
  result = fourOfaKind(cardDeck) ? result = 'Four of a Kind' : result;
  result = straightFlush(cardDeck) ? result = 'Straight Flush' : result;
  result = royalFlush(cardDeck) ? result = 'Royal Flush' : result;
  return result;
};


/**
 * @description Check for Jacks or Better
 * @param {Array} cardDeck Array with cards
 * @return {boolean} Result from checking
 * @example JacksOrBetter(["A♠", "10♥", "A♦", "10♠", "3♥"]) => true
 */
const jacksOrBetter = (cardDeck) => {
  const arr = cardDeck.map((x) => getCardNumber(x)).sort((a, b) => a - b);
  return arr.some((el, index) => arr.indexOf(el) !== index && el > 10);
};


/**
 * @description Check for Two Pairs
 * @param {Array} cardDeck Array with cards
 * @return {boolean} Result from checking
 */
const twoPairs = (cardDeck) => {
  const arr = cardDeck.map((x) => getCardNumber(x)).sort((a, b) => a - b);
  const result = arr.reduce((acc, el, index) => {
    if (el === arr[index + 1] && !acc.includes(el) ) {
      acc.push(el);
    }
    return acc;
  }, []);

  if (result.length > 1) {
    return true;
  }
  return false;
};


/**
 * @description Check for Three of the Kind
 * @param {Array} cardDeck Array with cards
 * @return {boolean} Result from checking
 */
const threeOfaKind = (cardDeck = []) => {
  const arr = cardDeck.map((x) => getCardNumber(x)).sort((a, b) => a - b);
  return arr.some((el) => {
    const first = arr.indexOf(el);
    const second = arr.lastIndexOf(el);
    return first !== second && (second - first) === 2;
  });
};


/**
 * @description Check for Straight
 * @param {Array} cardDeck Array with cards
 * @return {boolean} Result from checking
 */
const straight = (cardDeck = []) => {
  const arr = cardDeck.map((x) => getCardNumber(x)).sort((a, b) => a - b);
  return arr.every((el, index) => index === arr.indexOf(el)) &&
    arr[arr.length - 1] - arr[0] === 4;
};


/**
 * @description Check for Flush
 * @param {Array} cardDeck Array with cards
 * @return {boolean} Result from checking
 */
const flush = (cardDeck = []) => {
  const arr = cardDeck.map((x) => getCardColor(x));
  return arr.every((el) => el === arr[0]);
};


/**
 * @description Check for Full House
 * @param {Array} cardDeck Array with cards
 * @return {boolean} Result from checking
 */
const fullHouse = (cardDeck = []) => {
  const arr = cardDeck.map((x) => getCardNumber(x)).sort((a, b) => a - b);
  const a = arr[0] === arr[1] && arr[2] === arr[4];
  const b = arr[0] === arr[2] && arr[3] === arr[4];
  return a || b;
};


/**
 * @description Check for Four of the Kind
 * @param {Array} cardDeck Array with cards
 * @return {boolean} Result from checking
 */
const fourOfaKind = (cardDeck = []) => {
  const arr = cardDeck.map((x) => getCardNumber(x)).sort((a, b) => a - b);
  return arr.some((el) => {
    const first = arr.indexOf(el);
    const second = arr.lastIndexOf(el);
    return first !== second && (second - first) === 3;
  });
};


/**
 * @description Check for Straight Flush
 * @param {Array} cardDeck Array with cards
 * @return {boolean} Result from checking
 */
const straightFlush = (cardDeck = []) => {
  const arr = cardDeck.map((x) => getCardNumber(x)).sort((a, b) => a - b);
  return straight(cardDeck) && flush(cardDeck) && arr[arr.length - 1] < 14;
};

/**
 * @description Check for Straight Flush
 * @param {Array} cardDeck Array with cards
 * @return {boolean} Result from checking
 */
const royalFlush = (cardDeck = []) => {
  const arr = cardDeck.map((x) => getCardNumber(x)).sort((a, b) => a - b);
  return straight(cardDeck) && flush(cardDeck) && arr[arr.length - 1] === 14;
};


// checkCombinations(['9♠', '3♥', 'A♦', '3♠', '3♣']);
// checkCombinations(['2♠', '4♥', '4♦', '4♠', '4♣']);


// checkCombinations(['10♠', 'J♠', 'K♠', 'Q♠', 'A♠']);
