/* eslint-disable no-return-assign */
import * as cons from '../common/constants.js';
import { insertHTML, clearHTML, waitSeconds } from '../data/help-functions.js';
import { getCards } from '../data/cards.js';
import { addCardsToDesk, addSecondCards } from '../views/cards-views.js';
import { changeCards } from './card-events.js';
import { checkCombinations } from '../calculations/check-combs.js';
import { selectCombination } from '../views/info-views.js';
import { calculateBank } from '../calculations/calculate-bank.js';


/**
* @description Add a coin to table and increase bet with 100
*/
export const betOne = () => {
  if (amounts.bet < cons.MAX_BET) {
    amounts.bet = amounts.bet + 100;
    changeBet(amounts.bet);
    addCoin(amounts.bet / 100);
    setCurrentColumn(amounts.bet / 100);
    if (amounts.bet === cons.MAX_BET) {
      disableBets(true);
    }
  }
};


/**
* @description Add max coins to table and increase bet to max (default is 500)
*/
export const betMax = async () => {
  for (let i = amounts.bet + 100; i <= cons.MAX_BET; i = i + 100) {
    amounts.bet = i;
    await waitSeconds(500);
    changeBet(amounts.bet);
    addCoin(amounts.bet / 100);
    setCurrentColumn(amounts.bet / 100);
  }
  disableBets(true);
};


/**
* @description change style of the actual reward colum
* @param {Number} num the number of column
*/
const setCurrentColumn = (num) => {
  let myCol = document.querySelectorAll(`.selectedPos`);
  myCol.forEach((x) => x.classList = `pos0${num-1}`);
  myCol.forEach((x) => x.style.backgroundColor = '');

  myCol = document.querySelectorAll(`.pos0${num}`);
  myCol.forEach((x) => x.classList = 'selectedPos');
  myCol.forEach((x) => x.style.backgroundColor = 'green');
};


/**
* @description Change the bet and bank on the screen
* @param {number} num the current bet
* @return {void}
*/
const changeBet = (num) => {
  insertHTML('betValue', `${cons.BET_TEXT}${num}`);

  amounts.bank = amounts.bank - 100;
  insertHTML('bankValue', `${cons.BANK_TEXT}${amounts.bank}`);

  document.getElementById('draw').disabled = false;
  document.getElementById('draw').classList = 'mainButton';
};

/**
* @description Change enabling to bet buttons
* @param {boolean} enable enable or disable bet buttons
*/
const disableBets = (enable) => {
  if (enable) {
    document.getElementById('betOne').disabled = true;
    document.getElementById('betMax').disabled = true;
    document.getElementById('betOne').classList = 'mainButton_disabled';
    document.getElementById('betMax').classList = 'mainButton_disabled';
  } else {
    document.getElementById('betOne').disabled = false;
    document.getElementById('betMax').disabled = false;
    document.getElementById('betOne').classList = 'mainButton';
    document.getElementById('betMax').classList = 'mainButton';
  }
};


/**
* @description Prepare all variables and objects for game
*/
export const startGame = () => {
  insertHTML('betValue', `${cons.BET_TEXT}0`);
  insertHTML('bankValue', `${cons.BANK_TEXT}${cons.START_BANK}`);
  clearHTML('chipsPlace');
  disableBets(false);
  document.getElementById('draw').disabled = true;
  document.getElementById('draw').classList = 'mainButton_disabled';
};


const newDraw = () => {
  clearHTML('chipsPlace');
  disableBets(false);
  document.getElementById('draw').disabled = true;
  document.getElementById('draw').classList = 'mainButton_disabled';
  clearHTML('cardsPlace');
  amounts.bet = 0;
  myDeck.step = 0;
  myDeck.changeDeck = [];
  myDeck.extraDeck = [];
  myDeck.finalDeck = [];
  myDeck.firstDeck = [];
  insertHTML('betValue', `${cons.BET_TEXT}0`);
  insertHTML('bankValue', `${cons.BANK_TEXT}${amounts.bank}`);
  const table = document.querySelectorAll('.infoRow');
  table.forEach((x) => {
    x.style.backgroundColor = '';
    const elka = x.querySelectorAll('span');
    elka.forEach((el, index) => {
      if (index > 0) {
        el.classList = `pos0${index}`;
        el.style.backgroundColor = '';
      }
    }
    );
  });
};


/**
* @description Create coin on table
* @param {Number} num ID number for coin
*/
const addCoin = (num) => {
  const myCard = document.createElement('img');
  myCard.id = `coin${num}`;
  myCard.src = `./img/chip${num}.svg`;
  myCard.classList = 'coins';
  document.getElementById('chipsPlace').append(myCard);
};


export const drawFirstCards = () => {
  disableBets(true);
  if (myDeck.step === 0) {
    const allCards = getCards();
    myDeck.firstDeck = allCards.slice(0, 5);
    myDeck.extraDeck = allCards.slice(5, 10);
    addCardsToDesk(myDeck.firstDeck);
    myDeck.step = 1;
  } else if (myDeck.step === 1) {
    if (myDeck.changeDeck.length === 0) {
      if (confirm('NOT NEED NEW CARDS?')) {
        myDeck.step = 2;
        myDeck.finalDeck = myDeck.firstDeck;
      } else {
        return;
      }
    } else {
      myDeck.step = 2;
      changeCards();
      addSecondCards(myDeck.firstDeck, myDeck.finalDeck);
    }
    const result = checkCombinations(myDeck.finalDeck);
    selectCombination(result);
    amounts.bank = amounts.bank + calculateBank(result, amounts.bet);
  } else {
    newDraw();
  }
};

/**
* @description Revealing module pattern
* @return {Object} Object with two variables
*/
export const amounts = (() => {
  const bank = cons.START_BANK;
  const bet = 0;
  return {
    bank,
    bet,
  };
})();


/**
* @description Revealing module pattern
* @return {Object} Object with two arrays
*/
export const myDeck = (() => {
  const step = 0;
  const firstDeck = [];
  const extraDeck = [];
  const changeDeck = [];
  const finalDeck = [];
  return {
    step,
    firstDeck,
    extraDeck,
    finalDeck,
    changeDeck,
  };
})();
