import { getCardImage } from '../data/cards.js';
import { clearHTML, waitSeconds } from '../data/help-functions.js';

export const addCardsToDesk = async (deck) => {
  clearHTML('cardsPlace');
  for (let i = 0; i < deck.length; i++) {
    await waitSeconds(300);
    const card = document.createElement('img');
    card.src = getCardImage(deck[i]);
    card.classList = 'cards';
    card.id = `card_0${i + 1}`;
    card.setAttribute('name', deck[i]);
    card.style.gridArea = `card0${i + 1}`;
    document.getElementById('cardsPlace').append(card);
  }
};

export const addSecondCards = async (original = [], final = []) => {
  for (let i = 0; i < final.length; i++) {
    if (original[i] !== final[i]) {
      document.getElementById(`card_0${i + 1}`).remove();
      await waitSeconds(300);
      const card = document.createElement('img');
      card.src = getCardImage(final[i]);
      card.classList = 'cards';
      card.id = `card_0${i + 1}`;
      card.setAttribute('name', final[i]);
      card.style.gridArea = `card0${i + 1}`;
      document.getElementById('cardsPlace').append(card);
    }
  }
};
