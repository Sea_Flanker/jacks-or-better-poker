export const selectCombination = (row) => {
  if (row === 'nothing') {
    return;
  }

  const result = row.toUpperCase().split(' ').join('_');
  let winLine = document.getElementById(result).style.backgroundColor = 'green';

  winLine = document.getElementById(result).querySelectorAll('span');
  winLine.forEach((x) => {
    x.style.backgroundColor = '';
  });
};
